﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cardboard.Server;
using Cardboard.Server.Models;
using System.Net.Http;

namespace Cardboard.WebApp
{
    public class TestCardService: ICardService
    {
        public IEnumerable<Card> GetCards(HttpRequestMessage request, string boardId)
        {
            return new Card[] 
            {
                new Card()
                {
                    Id = boardId + "1",
                    Owner = "Federico",
                    Priority = 1,
                    Title = boardId + "Task 1",
                    Status = "In Progress"
                },
                new Card()
                {
                    Id = boardId + "2",
                    Owner = "Chris",
                    Priority = 1,
                    Title = boardId + "Task 2",
                    Status = "Todo"
                },
                new Card()
                {
                    Id = boardId + "3",
                    Owner = "Alvaro",
                    Priority = 1,
                    Title = boardId + "Task 3",
                    Status = "Done"
                }
            };
        }
    }
}