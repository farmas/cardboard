/// <reference path="typings/cardboard/cardboard.d.ts" />
$(function () {
    var groupByList = [
        {
            name: "Owner",
            cardProperty: "owner",
            cardValues: [
                "Federico",
                "Chris",
                "Alvaro"
            ]
        },
        {
            name: "Priority",
            cardProperty: "priority"
        }
    ], columns = [
        {
            name: "",
            width: "10%"
        },
        {
            name: "Todo",
            width: "40%",
            showCellCount: true
        },
        {
            name: "In Progress",
            width: "10%"
        },
        {
            name: "Done",
            width: "40%"
        }
    ];

    $("#board").cardboard({
        boards: [
            {
                id: "Default",
                groupByList: groupByList,
                columns: columns
            },
            {
                id: "Secondary",
                groupByList: groupByList,
                columns: columns
            }]
    });
});
