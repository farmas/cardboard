///<reference path="TypeDefinitions.d.ts" />

import TestData = require("./TestData");
import Board = require("Cardboard.Client/Board");

export module Main {
    var groupByList: BoardRowsGroupBy[] = [
        {
            name: "Owner",
            cardProperty: "owner",
            cardValues: TestData.teamMembers
        },
        {
            name: "Priority",
            cardProperty: "priority"
        }]

    Board.init($("#board"), <BoardCollectionSettings>{
        getCardsAsync: (): JQueryPromise<BoardCardData[]> => {
            var deferred = $.Deferred<any>();

            window.setTimeout(() => {
                deferred.resolve(TestData.getTesCards());
            }, 1000);

            return deferred.promise();
        },
        boards: [
            {
                id: "Default",
                groupByList: groupByList,
                columns: [
                    {
                        name: "",
                        width: "10%"
                    },
                    {
                        name: "Todo",
                        width: "40%",
                        showCellCount: true
                    },
                    {
                        name: "In Progress",
                        width: "25%"
                    },
                    {
                        name: "Done",
                        width: "25%"
                    }
                ]
            },
            {
                id: "Secondary",
                groupByList: groupByList,
                columns: [
                    {
                        name: "",
                        width: "10%"
                    },
                    {
                        name: "Todo",
                        width: "40%",
                        showCellCount: true
                    },
                    {
                        name: "In Progress",
                        width: "25%"
                    },
                    {
                        name: "In Review",
                        width: "25%"
                    }
                ]
            }]
    });
}