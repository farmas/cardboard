///<reference path="../../Scripts/typings/cardboard/cardboard.d.ts" />
///<reference path="../../Scripts/typings/knockout/knockout.d.ts" />
///<reference path="../../Scripts/typings/underscore/underscore.d.ts" />

interface KnockoutBindingHandlers {
    card: KnockoutBindingHandler;
}

interface BoardSettings extends BoardClientSettings, BoardServerSettings {
}