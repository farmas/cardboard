///<reference path="../TypeDefinitions.d.ts" />

export = Main;

module Main {
    export interface Identifiable {
        id: string;
    }

    export class List<T extends Identifiable> {
        public items = ko.observableArray<T>();
        public find(id: string): T {
            return _.find<T>(this.items.peek(), i => i.id === id);
        }
    }
}