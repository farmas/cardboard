///<reference path="../TypeDefinitions.d.ts" />

import Identity = require("./Identity");

export = Main;

module Main {

    var $ = jQuery,
        labelCss = "" +
            "\"label-default\": label().type === 1," +
            "\"label-primary\": label().type === 2," +
            "\"label-warning\": label().type === 3," +
            "\"label-danger\": label().type === 4,",
        template = "" +
        "<div class='card'>" +
            "<div class='card-title'>" +
                "<a data-bind='text: data.id, attr: { href: url }'></a>" +
                "<span class='label label-primary' data-bind='text: label().text, css: { "+ labelCss +" }'></span>" +
            "</div>" +
            "<div class='card-description' data-bind='text: data.title'></div>" +
        "</div>";

    export enum LabelType {
        None = 0,
        Default = 1,
        Primary = 2,
        Warning = 3,
        Danger = 4
    }

    export interface Label {
        text: string;
        type: LabelType;
    }

    export class ViewModel implements Identity.Identifiable {
        public data: BoardCardData;
        public url: string;
        public label: KnockoutComputed<Label>;

        constructor(data: BoardCardData, groupBy: KnockoutObservable<BoardRowsGroupBy>, settings: BoardSettings) {
            this.data = data;
            this.url = settings.getCardUrl(data, settings);
            this.label = ko.computed<Label>(() => {
                return this._getLabel(groupBy());
            });
        }

        public get id(): string {
            return this.data.id;
        }

        private _getLabel(groupBy: BoardRowsGroupBy): Label {
            var orderByProperty = groupBy.cardProperty,
                label: Label = {
                    text: "",
                    type: LabelType.None
                };

            if (orderByProperty === "priority") {
                label.text = this.data.owner;
                label.type = LabelType.Primary;
            } else if (orderByProperty === "owner") {
                label.text = "Pri: " + this.data.priority;
                switch (this.data.priority) {
                    case 0:
                        label.type = LabelType.Danger;
                        break;
                    case 1:
                        label.type = LabelType.Warning;
                        break;
                    default:
                        label.type = LabelType.Primary;
                        break;
                }
            }

            return label;
        }
    }

    ko.bindingHandlers.card = {
        init: (element: any): void => {
            $(element).html(template);
        }
    }
}