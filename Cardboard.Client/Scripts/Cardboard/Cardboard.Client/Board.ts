///<reference path="../TypeDefinitions.d.ts" />

import Card = require("./Card");
import Identity = require("./Identity");

export = Main;

module Main {

    var $ = jQuery,
        classNames = {
            boardHeaderFixed: "board-header-fixed"
        },
        defaultGroupBy: BoardRowsGroupBy = {
            name: "Owner",
            cardProperty: "owner"
        },
        defaultClientSettings: BoardClientSettings = {
            id: "Default",
            columns: [],
            groupByList: [defaultGroupBy]
        },
        defaultServerSettings: BoardServerSettings = {
            getCardsAsync: (board: BoardClientSettings, queryString: string): JQueryPromise<any> => {
                return $.ajax("api/Cards?" + queryString, {
                    dataType: "json"
                });
            },
            getCardUrl: (card: BoardCardData, board: BoardClientSettings): string => {
                return "http://myurl/" + board.id + card.id;
            }
        },
        template = "" +
        "<div class='board-header'>" +
        "</div>" +
        "<div class='board-controls'>" +
            "<div class='form-inline navbar-right'>" +
                "<div class='form-group board-filter'>" +
                    "<label>Board:</label>" +
                    "<select class='form-control' data-bind='options: items, optionsText: \"name\", value: selectedBoard'></select>" +
                "</div>" +
                "<div class='form-group board-filter' data-bind='with: selectedBoard'>" +
                    "<label>Group By:</label>" +
                    "<select class='form-control' data-bind='options: settings.groupByList, optionsText: \"name\", value: selectedGroupBy'></select>" +
                "</div>" +
            "</div>" +
        "</div>" +

        "<div class='board-content'>" +
            "<table class='table table-striped' data-bind='with: selectedBoard'>" +
                "<thead>" +
                "<tr data-bind='foreach: settings.columns'>" +
                    "<th data-bind='text: name, style: { width: width }'></th>" +
                "</tr>" +
                "</thead>" +
                "<tbody data-bind='foreach: items'>" +
                    "<tr class='board-row' data-bind='foreach: items'>" +
                        "<!-- ko if: $index() === 0 -->" +
                        "<td>" +
                            "<span data-bind='text: id'></span>" +
                            "<button type='button' class='btn btn-default board-button-collapse' data-bind='click: $parent.onCollapseClick'>"+
                                "<span class='glyphicon' data-bind='css: { \"glyphicon-plus\": $parent.collapsed, \"glyphicon-minus\": $parent.collapsed() === false }'></span>"+
                            "</button>" +
                        "</td>" +
                        "<!-- /ko -->" +
                        "<!-- ko if: $index() > 0 -->" +
                            "<td>" +
                                "<div class='board-cell-container'>" +
                                    "<span class='badge board-cell-badge' data-bind='visible: showCount, text: items().length'></span>" +
                                    "<div class='board-cell-content' data-bind='foreach: items, css: { \"board-cell-collapsed\": collapsed }'>" +
                                        "<div data-bind='card: $data'></div>" +
                                    "</div>" +
                                    "<div class='board-cell-footer-gradient' data-bind='visible: $parent.collapsed'></div>" +
                                "</div>" +
                            "</td>" +
                        "<!-- /ko -->" +
                    "</tr>" +
                "</tbody>" +
            "</table>" +
        "</div>";

    export function init(parent: JQuery, settings: BoardCollectionSettings): void {
        if (!settings.boards || !settings.boards.length) {
            throw new Error("At least one board is required.");
        }

        var serverSetting = $.extend({}, defaultServerSettings, settings);

        var boards = settings.boards.map<Board>((boardSettings) => {
            var combinedSettings: BoardSettings = $.extend({}, serverSetting, boardSettings, <BoardServerSettings>{
                getCardsAsync: settings.getCardsAsync,
                getCardUrl: settings.getCardUrl
            });

            return new Board(combinedSettings);
        });

        var viewModel = new ViewModel(boards),
            startupBoard = viewModel.find(settings.startupBoardId) || viewModel.items[0];

        viewModel.selectedBoard(startupBoard);

        parent.html(template);
        ko.applyBindings(viewModel, parent[0]);

        setupEvents(parent);
    }

    function setupEvents(element: any): void {
        $(window).scroll(() => {
            var scrollTop = $(this).scrollTop();

            if (scrollTop > 40) {
                element.addClass(classNames.boardHeaderFixed);
            } else {
                element.removeClass(classNames.boardHeaderFixed);
            }
        });
        
    }

    class ViewModel extends Identity.List<Board> {
        public selectedBoard = ko.observable<Board>();

        constructor(boards: Board[]) {
            super();
            this.items(boards);

            this.selectedBoard.subscribe((board) => {
                board.load();
            });
        }
    }

    class Row extends Identity.List<Cell> implements Identity.Identifiable {
        public id: string
        public collapsed: KnockoutObservable<boolean>;

        constructor(id: string, boardSettings: BoardSettings) {
            super();
            this.id = id;
            this.collapsed = ko.observable<boolean>(!boardSettings.expandRows);    
        }

        public onCollapseClick = () => {
            this.collapsed(!this.collapsed());
        };
    }

    class Cell extends Identity.List<Card.ViewModel> implements Identity.Identifiable {
        public id: string
        public collapsed: KnockoutComputed<boolean>;
        public showCount: KnockoutObservable<boolean>

        constructor(id: string, parentRow: Row, columnSettings: BoardColumn) {
            super();
            this.id = id;
            this.showCount = ko.observable<boolean>(!!columnSettings.showCellCount);
            this.collapsed = ko.computed<boolean>(() => {
                return parentRow.collapsed();
            });
        }
    }

    class Board extends Identity.List<Row> implements Identity.Identifiable {
        public settings: BoardSettings;
        public cards: KnockoutObservableArray<BoardCardData>;
        public selectedGroupBy = ko.observable<BoardRowsGroupBy>(defaultGroupBy);
        public autoCreateRows = ko.computed(() => {
            return !this.selectedGroupBy().cardValues || this.selectedGroupBy().cardValues.length === 0;
        });

        constructor(settings: BoardSettings) {
            super();
            this.settings = settings;

            if (settings.groupByList && settings.groupByList.length > 0) {
                this.selectedGroupBy(settings.groupByList[0]);
            }

            this._setupSubscriptions();
        }

        public get id(): string {
            return this.settings.id;
        }

        public get name(): string {
            return this.settings.name || this.id;
        }

        public load(): void {
            this._createStartupRows();

            if (!this.cards) {
                var queryObject = $.extend({ boardId: this.settings.id }, this.settings.query),
                    queryString = this._buildQueryString(queryObject);
                
                this.settings.getCardsAsync(this.settings, queryString).done((cards) => {
                    this.cards = ko.observableArray<BoardCardData>(cards);
                    this._putCardsOnBoard();
                });
            } else {
                this._putCardsOnBoard();
            }
        }

        private _buildQueryString(obj: Object): string {
            var queryString: string[] = [];

            if (obj) {
                Object.keys(obj).forEach((prop) => {
                    queryString.push(encodeURIComponent(prop) + "=" + encodeURIComponent(obj[prop]));
                });
            }

            return queryString.join("&");
        }

        private _setupSubscriptions(): void {
            this.selectedGroupBy.subscribe((newGroupBy) => {
                this.items.splice(0, this.items().length);
                this._putCardsOnBoard();
            });
        }

        private _createStartupRows(): void {
            this.items.splice(0, this.items().length);

            if (!this.autoCreateRows()) {
                this.selectedGroupBy().cardValues.forEach((rowId) => {
                    this._createRow(rowId);
                });
            }
        }

        private _putCardsOnBoard(): void {
            this._createStartupRows();

            this.cards().forEach((card) => {
                var rowId = card[this.selectedGroupBy().cardProperty],
                    row = this.find(rowId),
                    cell: Cell;

                if (!row && (rowId !== undefined) && this.autoCreateRows()) {
                    row = this._createRow(rowId);
                }

                cell = row && row.find(card.status);
                if (cell) {
                    cell.items.push(new Card.ViewModel(card, this.selectedGroupBy, this.settings));
                }
            });

            this._sortCards();
        }

        private _sortCards(): void {
            // sort rows.
            if (this.autoCreateRows()) {
                this.items.sort((left, right) => {
                    if (left.id > right.id) {
                        return 1;
                    } else if (left.id < right.id) {
                        return -1;
                    }
                    return 0;
                });
            }

            // sort cells
            if (this.selectedGroupBy().cardProperty !== "priority") {
                this.items().forEach((row) => {
                    row.items().forEach((cell) => {
                        cell.items.sort((left, right) => {
                            return left.data.priority - right.data.priority;
                        });
                    });
                });
            }
        }

        private _createRow(rowId: string): Row {
            var row = new Row(rowId, this.settings);

            this.settings.columns.forEach((columnSetting, index) => {
                var cellId = index === 0 ? rowId : columnSetting.name,
                    cell = new Cell(cellId, row, columnSetting);

                row.items.push(cell);
            });

            this.items.push(row);
            return row;
        }
    }

    jQuery.fn.cardboard = function (settings: BoardCollectionSettings) {
        return (<JQuery>this).each((i, e) => {
            init($(this), settings);
        });
    };
}
