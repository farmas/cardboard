///<reference path="TypeDefinitions.d.ts" />

export = Main;

module Main {
    var cardCount = 0,
        cardTitles = [
            "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent dapibus ullamcorper ipsum, sit amet hendrerit enim.",
            "Nam non scelerisque arcu. Nam varius facilisis magna, molestie ultrices nisl. Suspendisse semper convallis justo, sed porta velit sollicitudin id.",
            "In eget vestibulum metus. Aliquam id est ac metus malesuada volutpat quis quis justo. Donec varius vitae erat nec vehicula."
        ];

    export var teamMembers = ["Alvaro", "Brad", "Chris", "David", "Federico", "Mo", "Steve", "Tom", "William"];
    export var statuses = ["Todo", "In Progress", "Done"];
    export var priorities = [0, 1, 2];

    export function getTesCards(): BoardCardData[]{
        var cards: BoardCardData[] = [];

        for (var i = 0; i < 100; i++) {
            cards.push(_createCard());
        }

        return cards;
    }

    function _getRandomValue<T>(arr: T[]): T {
        var size = arr.length,
            index = Math.floor(Math.random() * size);

        return arr[index];
    }
        
    function _createCard(): BoardCardData {
        return {
            id: (++cardCount).toString(),
            title: _getRandomValue(cardTitles),
            owner: _getRandomValue(teamMembers),
            priority: _getRandomValue(priorities),
            status: _getRandomValue(statuses)
        };
    }
}