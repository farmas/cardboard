﻿{
    //appDir: "../Cardboard/Cardboard.Client",
    baseUrl: ".",
    name: "Scripts/almond.js",
    include: ["Scripts/Cardboard/Cardboard.Client/Board"],
    out: "../Cardboard/Cardboard.Client/cardboard.js",
    wrap: {
        startFile: "start.frag",
        endFile: "end.frag"
    }
}