﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cardboard.Server.Models;

namespace Cardboard.Server.Controllers
{
    public class CardsController : ApiController
    {
        private readonly ICardService _cardService;

        public CardsController(ICardService cardService)
        {
            this._cardService = cardService;
        }

        public IEnumerable<Card> GetCards(string boardId)
        {
            return this._cardService.GetCards(this.Request, boardId);
        }
    }
}
