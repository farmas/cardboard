﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Ninject;
using WebApiContrib.IoC.Ninject;

namespace Cardboard.Server
{
    public static class NinjectConfig
    {
        public static void Register(HttpConfiguration config, Action<IKernel> configure)
        {
            IKernel kernel = new StandardKernel();

            System.Web.Http.Dependencies.IDependencyResolver ninjectResolver = new NinjectResolver(kernel);
            config.DependencyResolver = ninjectResolver;

            configure(kernel);
        }
    }
}