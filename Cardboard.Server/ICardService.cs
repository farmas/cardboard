﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cardboard.Server.Models;
using System.Net.Http;

namespace Cardboard.Server
{
    /// <summary>
    /// Service for a cardboad server.
    /// </summary>
    public interface ICardService
    {
        /// <summary>
        /// Gets the cards for a board.
        /// </summary>
        /// <param name="request">Current request object.</param>
        /// <param name="boardId">Identifier of the board.</param>
        /// <returns>List of cards.</returns>
        IEnumerable<Card> GetCards(HttpRequestMessage request, string boardId);
    }
}
